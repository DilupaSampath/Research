import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
interface Doctor {
  name: string;
  ward: string;
  assingDate: string;
  priority: string;
}

@Component({
  selector: 'app-profile1',
  templateUrl: './profile1.component.html',
  styleUrls: ['./profile1.component.scss']
})

export class Profile1Component implements OnInit {
  Projects:any[];
doctors:Doctor[];  
name: any;
ward: any;
assingDate: any;
priority: any;
title:any;
buttonTitle:any;
indexDoctro:any;
removeId:any;
statusRemove=false;

  constructor(private http: HttpClient) {
    // this.doctors= doctorServices.doctors;
    this.title="New Doctor";
    this.buttonTitle="Assign Doctor";

   }

  ngOnInit() {    
    this.getDoctors()

    // this.doctors= this.doctorServices.doctors;
  }
  getDoctors() {
    this.http.get('http://172.28.1.46:5000/doctors/')
      .subscribe(
        (data: any[]) => {
          console.log("an init works--> " + JSON.stringify(data));
          this.doctors = data;
         
        }
      );
  }
  onCreate(){
    //  console.log("doctor added...!"+this.name);
    // this.doctorServices.onCreateDocter(this.name,this.ward,this.assingDate,this.priority);
    let formData = {
      'name':this.name,
      'ward':this.ward,
      'assingDate':this.assingDate,
      'priority':this.priority
    };

    // console.log("doctor added...!");
    this.http.post('http://172.28.1.46:5000/doctors/', formData).subscribe(
      (data: any) => {
        console.log(data);
        this.getDoctors();
      });
        
  }
  onUpdate(){
// this.doctorServices.onUpdateDoctor(this.indexDoctro,this.name,this.ward,this.assingDate,this.priority);
let updateData= {
  'name': this.name,
  'ward':this.ward,
  'assingDate':this.assingDate,
  'priority':this.priority
};
this.http.patch('http://172.28.1.46:5000/doctors/', updateData, {}).subscribe(
  (data: any) => {
    this.getDoctors();
    console.log(data);
  }
)
  }
  setInsetTitle(){
    this.title="New Doctor";
      }
  setToForm(index:any){
    this.indexDoctro=index;
    this.buttonTitle="Save Changes";
    this.title="Update Doctor";
    this.name=this.doctors[index].name;
    this.ward=this.doctors[index].ward;
    this.assingDate=this.doctors[index].assingDate;
    this.priority=this.doctors[index].priority;
    console.log(this.doctors[index]);

  }
  onRemove(){
    // this.doctorServices.removeDoctor(this.removeIndex);
    // this.http.post('http://localhost:5000/doctors/', formData).subscribe(
    //   (data: any) => {
    //     console.log(data);
    //     this.getDoctors();});

    this.http.request('DELETE','http://172.28.1.46:5000/doctors/' , { 'body':{'name':this.removeId}}).subscribe(
      (data: any[]) => {
        console.log(data);
        this.getDoctors();


      }

    );
    // this.adminServices.deleteProject(index);

  }
  onRemoveIndex(index:any){
    this.removeId = index;
    // this.removeIndex=index;
  }
  launch_toast() {
    var x = document.getElementById("toast")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}
launch_toast2() {
  var x = document.getElementById("toast1")
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}

}
