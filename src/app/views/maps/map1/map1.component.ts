import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
interface Patient {
  id:any;
  NIC:any,
  name:any;
  address:any;
  gender:any;
  distric:any;
  date:any;
  level:any;
  ward:any;
  priority:any;
  comments:any;
}
@Component({
  selector: 'app-map1',
  templateUrl: './map1.component.html',
  styleUrls: ['./map1.component.scss']
})
export class Map1Component implements OnInit {
  public map: any = { lat: 51.678418, lng: 7.809007 };
  patients:Patient[];
  id:any
  NIC:any;
  name:any;
  address:any;
  gender:any;
  distric:any;
  date:any;
  level:any;
  ward:any;
  priority:any;
  comments:any;
  title:any;
buttonTitle:any;
indexPatient:any;
removeId:any;
statusRemove=false;

  constructor(private http: HttpClient) { 
  
    this.title="New Patient";
    this.buttonTitle="Assign Patient";
  }
  ngOnInit() {
    this.getPatients();
  }
  getPatients() {
    this.http.get('http://172.28.1.46:5000/patients/')
      .subscribe(
        (data: any[]) => {
          console.log("an init works--> " + JSON.stringify(data));
          this.patients = data;
         
        }
      );
  }
  onCreate(){
    let currentTime = new Date()

    console.log(this.name+" ***");
    this.id=(currentTime.getFullYear().toString().substring(2))+this.NIC;
    console.log(this.id);
    let formData= {
      "id":this.id,
      "NIC": this.NIC,
      "address":this.address,
      "comments": this.comments,
      "date": this.date,
      "distric": this.distric,
      "gender": this.gender,
      "level": this.level,
      "name": this.name,
      "priority": this.priority,
      "ward": this.ward
    };


    this.http.post('http://172.28.1.46:5000/patients/', formData).subscribe(
      (data: any) => {
        console.log(data);
        this.getPatients();
      });
        

    // this.patientrServices.onCreatePatient(this.id,this.name,this.address,this.gender,this.distric,this.date,this.level,this.ward,this.priority,this.comments);
  }
  onUpdate(){
    let updateData= {
      "NIC": this.NIC,
      "address":this.address,
      "comments": this.comments,
      "date": this.date,
      "distric": this.distric,
      "gender": this.gender,
      "id": this.id,
      "level": this.level,
      "name": this.name,
      "priority": this.priority,
      "ward": this.ward
    };
    console.log(this.id);
    this.http.patch('http://172.28.1.46:5000/patients/', updateData, {}).subscribe(
      (data: any) => {
        this.getPatients();
        console.log(data);
      }
    )
      this.launch_toast2();
      }
      setInsetTitle(){
        this.title="New Patient";
          }
      setToForm(index:any){
        this.indexPatient=index;
        this.buttonTitle="Save Changes";
        this.title="Update Patient";
        this.name=this.patients[index].name;
        this.NIC=this.patients[index].NIC,
        this.address=this.patients[index].address;
        this.gender=this.patients[index].gender;
        this.distric=this.patients[index].distric;
        this.date=this.patients[index].date;
        this.level=this.patients[index].level;
        this.ward=this.patients[index].ward;
        this.priority=this.patients[index].priority;
        this.comments=this.patients[index].comments;
      }
      onRemove(){
        // this.patientrServices.removePatient(this.removeIndex);
        this.http.request('DELETE','http://172.28.1.46:5000/patients/' , { 'body':{'id':this.removeId}}).subscribe(
          (data: any[]) => {
            console.log(data);
            this.getPatients();
    
    
          }
    
        );
    
      }
      onRemoveIndex(id:any){
        this.removeId=id;
      }
      launch_toast() {
        var x = document.getElementById("toast")
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
    }
    launch_toast2() {
      var x = document.getElementById("toast1")
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
    }
}
